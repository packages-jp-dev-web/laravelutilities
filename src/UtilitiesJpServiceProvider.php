<?php

namespace UtilitiesJp;

use Illuminate\Support\ServiceProvider;

class UtilitiesJpServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //$this->publishes([__DIR__ . "/classes" => base_path('app')]);
        $this->publishes([__DIR__ . "/assets" => base_path('public').'/assets']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    

}
