/* required cleave - 1.3.3 
 * required cleave-phone.br
 * maskedJp.js - 1.0.0
 * Copyright (C) 2018 Jefferson
 */

function maskedPhone(id) {
    new Cleave(id.toString(), {
        phone: true,
        phoneRegionCode: 'BR'
    });
}
function maskedCep(id) {
    new Cleave(id, {
        numericOnly: true,
        delimiters: ['-'],
        blocks: [5, 3]
    });
}
function maskedCnpj(id) {
    new Cleave(id, {
        numericOnly: true,
        delimiters: ['.', '.', '/', '-'],
        blocks: [2, 3, 3, 4, 2]
    });
}
function maskedCpf(id) {
    new Cleave(id, {
        numericOnly: true,
        delimiters: ['.', '.', '-'],
        blocks: [3, 3, 3, 2]
    });
}
function maskedDate(id, format) {
    new Cleave(id, {
        date: true,
        datePattern: format
    });
}
function maskedTime(id) {
    new Cleave(id, {
        time: true,
        timePattern: ['h', 'm', 's']
    });
}
function maskedTimeFormat(id, format) {
    new Cleave(id, {
        time: true,
        timePattern: format
    });
}
function maskedNumeral(id) {
    new Cleave(id, {
        numeral: true,
        numeralDecimalMark: '',
        delimiter: ''
    });
}
function maskedNumeralDecimalNotPoint(id) {
    new Cleave(id, {
        numeral: true,
        numeralDecimalMark: '.',
        delimiter: ''
    });
}
function maskedNumeralDecimal(id) {
    new Cleave(id, {
        numeral: true,
        numeralDecimalMark: ',',
        delimiter: '.'
    });
}
function maskedNumeralDecimalNotFlutuant(id) {
    new Cleave(id, {
        numeral: true,
        numeralDecimalMark: ',',
        delimiter: '.'
    });
}




