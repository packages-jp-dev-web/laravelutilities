<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UtilitiesJp\ClassesUtilitys;

use UtilitiesJp\ClassesUtilitys\EncryptUtility;

/**
 * Description of StringUtility
 *
 * @author Jefferson
 */
class StringUtility
{
    public static function generateSlugOfText($text)
    {
        $textAux = utf8_encode($text . EncryptUtility::numHash(rand(11111, 99999)));
        $string = \URLify::filter($textAux);
        while (empty($string)) {
            $string = \URLify::filter($textAux);
        }
        return $string;
    }
    public static function generateSlugOfTextWithComplement($text)
    {
        $textAux = utf8_encode($text);
        $string = \URLify::filter($textAux);
        while (empty($string)) {
            $string = \URLify::filter($textAux);
        }
        return $string;
    }
}