<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UtilitiesJp\ClassesUtilitys;

//Exception
use Symfony\Component\HttpFoundation\File\Exception\FileException;
//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;

/**
 * Description of Upload
 *
 * @author Jefferson
 */
class Upload
{

    public static function upload($file, $name, $way)
    {
        try {
            $namePhotoFormated = \URLify::filter($name);
            if (!$file->isValid()) {
                return null;
            }
            $destinationPath = $way; // upload path
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $fileName = $namePhotoFormated . time() . '.' . $extension; // renameing image
            $uploadSucess = $file->move($destinationPath, $fileName); // uploading file to given path
            if ($uploadSucess) {
                return $destinationPath . '/' . $fileName;
            } else {
                return null;
            }
        } catch (FileException $ex) {
            $log = new LogsSystem();
            $log->writeLog($ex->getMessage() . 'App\UtilitiesJp\Upload - upload');
            return null;
        }
    }
}