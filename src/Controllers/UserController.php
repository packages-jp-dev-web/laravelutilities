<?php

namespace UtilitiesJp\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Services
use UtilitiesJp\Services\UserService;

class UserController extends Controller {

    public function postEditUser(Request $request, UserService $userService) {
        $this->validate($request, $userService->getRules('edit', ['id' => auth()->user()->id]), $userService->getMessages());
        $data = $request->all();
        $data['id'] = auth()->user()->id;
        if ($userService->edit($data)) {
            return redirect()->back()->with('success', 'Os dados do perfil foram salvos.');
        }
        return redirect()->back()->with('error', 'Erro na solicitação, tente novamente mais tarde.');
    }

}
