<?php

namespace Tests\Feature;

//use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\TestCase;
use UtilitiesJp\ClassesUtilitys\Util;

class UtilTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testStrReplaceUtil() {
        for ($i = 0; $i < 100; $i++) {
            $string = random_bytes(30);
            $stringSub = substr($string, 10);
            $stringFinal = Util::str_replaceUtil($stringSub, '', $string);
            $rest = substr($string, -20);
            $this->assertEquals($rest, $stringFinal);
        }
        for ($i = 0; $i < 100; $i++) {
            $string = random_bytes(30);
            $stringSub = substr($string, -10);
            $stringFinal = Util::str_replaceUtil($stringSub, '', $string);
            $rest = substr($string, 20);
            $this->assertEquals($rest, $stringFinal);
        }
    }

    public function testImplodeUtil() {
        for ($i = 0; $i < 100; $i++) {
            $string = '';
            for ($i = 0; $i < 10; $i++) {
                $string = $string. rand(1, 9999).';';
            }
            $vector = Util::implodeUtil(';', $string);
            $this->assertEquals(10, count($vector));
        }
    }

}
